# ecl-plus-network-emulation

This aims to realize [BGP/EVPN](https://tools.ietf.org/html/rfc7432) and VxLAN

![ecl plus](images/ecl-plus-network-emulation.001.png)

If you want to know more detail informantion, please check out [オープンソースで学ぶ PrivateCloudネットワーク技術](https://speakerdeck.com/ttsubo/opunsosudexue-bu-privatecloudnetutowakuji-shu)

## Requirement

The following softwares must be installed in advance on Ubuntu environment.

- docker
- python3.x, pip

## Preparations
You need to get some docker images

1. build docker image for goplane

    ```
    $ docker build -t ttsubo/goplane build/goplane
    ```

2. build docker image for frrouging

    ```
    $ docker build -t ttsubo/frrouting build/frrouting
    ```

3. build docker image for python_vrrp

    ```
    $ docker build -t ttsubo/python_vrrp build/python_vrrp
    ```

4. fetch gobgp docker image

    ```
    $ docker pull osrg/quagga:latest
    ```


## How to run
You need to execute following five steps.

1. Change iptables for Bridge Netfilter

    ```
    $ sudo iptables -I FORWARD -m physdev --physdev-is-bridged -j ACCEPT
    ```

2. Install dependent python packages

    ```
    $ sudo pip install -r requires.txt
    ```

3. Start full of network components ( .. wait about 4 minutes for establishing BGP session )

    ```
    $ sudo python start-network.py
    ```

4. Start netconf server on docker of inet-gw

    ```
    $ docker exec -it inet-gw1 bash -c "/root/.pyenv/shims/python netconf_server.py"
    ```

    ```
    $ docker exec -it inet-gw2 bash -c "/root/.pyenv/shims/python netconf_server.py"
    ```

5. Apply netconf data for VRRP parameters

    ```
    $ docker exec -it inet-gw1 bash -c "/root/.pyenv/shims/python sample_vrrp1.py"
    ```

    ```
    $ docker exec -it inet-gw2 bash -c "/root/.pyenv/shims/python sample_vrrp2.py"
    ```


## How to confirm

As you see, you can confirm how these network works

![confirm](images/ecl-plus-network-emulation.002.png)

The following command is simple confirmation

confirm addressing on Ubuntu-2

```
$ docker exec -it ubuntu-2 ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
3260: eth0@if3261: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether aa:aa:aa:aa:aa:05 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 192.168.4.2/24 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::9c3c:64ff:fef3:6f2/64 scope link
       valid_lft forever preferred_lft forever
```

confirm reachability on end-to-end communications

```
$ docker exec -it ubuntu-2 ping 192.168.2.2 -I 192.168.4.2
PING 192.168.2.2 (192.168.2.2) from 192.168.4.2 : 56(84) bytes of data.
64 bytes from 192.168.2.2: icmp_seq=1 ttl=61 time=0.699 ms
64 bytes from 192.168.2.2: icmp_seq=2 ttl=61 time=0.733 ms
64 bytes from 192.168.2.2: icmp_seq=3 ttl=61 time=0.846 ms
^C
--- 192.168.2.2 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 1998ms
rtt min/avg/max/mdev = 0.699/0.759/0.846/0.066 ms
```

If you want to know current state of VRRP instance, please execute following command (username=user, password=1234)

```
$ docker exec -it inet-gw1 go run get_vrrp_information.go
Enter a valid username: user
Enter Password: 
VRRP Status Reply: 

<vrrp-information style="brief">

<vrrp-interface>
  <interface>eth0</interface>
  <interface-state>up</interface-state>
  <group>1</group>
  <vrrp-state>master</vrrp-state>
  <vrrp-mode>Active</vrrp-mode>
  <timer-name>A</timer-name>
  <timer-value>2.071</timer-value>
  <local-interface-address>192.168.3.2</local-interface-address>
  <virtual-ip-address>192.168.3.100</virtual-ip-address>
</vrrp-interface>

</vrrp-information>
```

```
$ docker exec -it inet-gw2 go run get_vrrp_information.go
Enter a valid username: user
Enter Password: 
VRRP Status Reply: 

<vrrp-information style="brief">

<vrrp-interface>
  <interface>eth0</interface>
  <interface-state>up</interface-state>
  <group>1</group>
  <vrrp-state>backup</vrrp-state>
  <vrrp-mode>Active</vrrp-mode>
  <timer-name>A</timer-name>
  <timer-value>2.071</timer-value>
  <local-interface-address>192.168.3.3</local-interface-address>
  <virtual-ip-address>192.168.3.100</virtual-ip-address>
</vrrp-interface>

</vrrp-information>
```

## How to stop
You need to execute following step

```
$ docker rm -f $(docker ps -a -q)
```
