# Copyright (C) 2015,2016 Nippon Telegraph and Telephone Corporation.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import sys
import itertools
import string
import random
import time

import netaddr
import toml
from pyroute2 import IPRoute

import textwrap
import shutil

try:
    from docker import Client
except ImportError:
    from docker import APIClient as Client
from nsenter import Namespace
from fabric.api import local

TEST_BASE_DIR = '/tmp/goplane'

IN6_ADDR_GEN_MODE_NONE = 1

dckr = Client()

flatten = lambda l: itertools.chain.from_iterable(l)
random_str = lambda n : ''.join([random.choice(string.ascii_letters + string.digits) for i in range(n)])
get_containers = lambda : [str(x)[1:] for x in list(flatten(n['Names'] for n in dckr.containers(all=True)))]

def indent(s):
    return textwrap.indent(str(s), ' '*4, lambda line: True)


class docker_netns(object):
    def __init__(self, name):
        pid = int(dckr.inspect_container(name)['State']['Pid'])
        if pid == 0:
            raise Exception('no container named {0}'.format(name))
        self.pid = pid

    def __enter__(self):
        pid = self.pid
        if not os.path.exists('/var/run/netns'):
            os.mkdir('/var/run/netns')
        os.symlink('/proc/{0}/ns/net'.format(pid), '/var/run/netns/{0}'.format(pid))
        return str(pid)

    def __exit__(self, type, value, traceback):
        pid = self.pid
        os.unlink('/var/run/netns/{0}'.format(pid))


class CmdBuffer(list):
    def __init__(self, delim='\n'):
        super(CmdBuffer, self).__init__()
        self.delim = delim

    def __lshift__(self, value):
        self.append(value)

    def __str__(self):
        return self.delim.join(self)


class Bridge(object):
    def __init__(self, name, subnet='', with_ip=True):
        ip = IPRoute()
        br = ip.link_lookup(ifname=name)
        if len(br) != 0:
            ip.link('del', index=br[0])
        ip.link('add', ifname=name, kind='bridge')
        br = ip.link_lookup(ifname=name)
        br = br[0]
        ip.link('set', index=br, IFLA_AF_SPEC={'attrs': [['AF_INET6', {'attrs': [['IFLA_INET6_ADDR_GEN_MODE', 1]]}]]})
        ip.link('set', index=br, state='up')

        if with_ip:
            self.subnet = netaddr.IPNetwork(subnet)

            def f():
                for host in self.subnet:
                    yield host
            self._ip_generator = f()
            # throw away first network address
#            self.next_ip_address()
            self.ip_addr = self.next_ip_address()
            address, prefixlen = self.ip_addr.split('/')
            ip.addr('add', index=br, address=address, prefixlen=int(prefixlen))

        self.name = name
        self.with_ip = with_ip
        self.br = br
        self.ctns = []

    def next_ip_address(self):
        return "{0}/{1}".format(next(self._ip_generator),
                                self.subnet.prefixlen)

    def addif(self, ctn, ifname='', mac=''):
        with docker_netns(ctn.name) as pid:
            host_ifname = '{0}_{1}'.format(self.name, ctn.name)
            guest_ifname = random_str(5)
            ip = IPRoute()
            ip.link('add', ifname=host_ifname, kind='veth', peer=guest_ifname)
            host = ip.link_lookup(ifname=host_ifname)[0]
            ip.link('set', index=host, master=self.br)
            ip.link('set', index=host, state='up')

            self.ctns.append(ctn)

            guest = ip.link_lookup(ifname=guest_ifname)[0]
            ip.link('set', index=guest, net_ns_fd=pid)
            with Namespace(pid, 'net'):
                ip = IPRoute()
                if ifname == '':
                    links = [x.get_attr('IFLA_IFNAME') for x in ip.get_links()]
                    n = [int(l[len('eth'):]) for l in links if l.startswith('eth')]
                    idx = 0
                    if len(n) > 0:
                        idx = max(n) + 1
                    ifname = 'eth{0}'.format(idx)
                ip.link('set', index=guest, ifname=ifname)
                ip.link('set', index=guest, state='up')

                if mac != '':
                    ip.link('set', index=guest, address=mac)

                if self.with_ip:
                    address, mask = self.next_ip_address().split('/')
                    ip.addr('add', index=guest, address=address, mask=int(mask))
                    ctn.ip_addrs.append((ifname, address, self.name))
            return ifname


class Container(object):
    def __init__(self, name, image):
        self.name = name
        self.image = image
        self.shared_volumes = []
        self.ip_addrs = []
        self.is_running = False
        if self.image in ["ttsubo/frrouting", "ttsubo/python_vrrp"]:
            config_dir = "{0}/{1}".format('/tmp/Frrouting', name)
            if os.path.exists(config_dir) is False:
                shutil.copytree('./conf/Frrouting', config_dir)
            self.shared_volumes.append((config_dir, "/etc/frr"))

    def run(self):
        if self.name in get_containers():
            self.stop()
        binds = ['{0}:{1}'.format(os.path.abspath(sv[0]), sv[1]) for sv in self.shared_volumes]
        config = dckr.create_host_config(binds=binds, privileged=True)
        ctn = dckr.create_container(image=self.image, detach=True, name=self.name,
                                    stdin_open=True, volumes=[sv[1] for sv in self.shared_volumes],
                                    host_config=config, network_disabled=True)
        dckr.start(container=self.name)
        self.id = ctn['Id']
        self.is_running = True
        with docker_netns(self.name) as pid:
            with Namespace(pid, 'net'):
                ip = IPRoute()
                lo = ip.link_lookup(ifname='lo')[0]
                ip.link('set', index=lo, state='up')


    def stop(self):
        dckr.remove_container(container=self.name, force=True)
        time.sleep(2)
        self.is_running = False

    def local(self, cmd, stream=False, detach=False):
        if stream:
            i = dckr.exec_create(container=self.name, cmd=cmd)
            return dckr.exec_start(i['Id'], tty=True, stream=stream, detach=detach)
        else:
            flag = '-d' if detach else ''
            return local('docker exec {0} {1} {2}'.format(flag, self.name, cmd), capture=True)

    def add_static_route(self, network, next_hop):
        c = CmdBuffer(' ')
        c << "vtysh -c 'configure terminal'"
        c << "-c 'ip route {0} {1}'".format(network, next_hop)
        self.local(str(c), detach=True)


class BGPContainer(Container):

    WAIT_FOR_BOOT = 0
    RETRY_INTERVAL = 5
    PEER_TYPE_INTERNAL = 'internal'
    PEER_TYPE_EXTERNAL = 'external'

    def __init__(self, name, asn, router_id, ctn_image_name='ttsubo/goplane',
                 log_level='debug', config_dir=None):
        if config_dir is None:
            self.config_dir = "{0}/{1}".format(TEST_BASE_DIR, name)
        else:
            self.config_dir = config_dir
        if not os.path.exists(self.config_dir):
            os.makedirs(self.config_dir)
            os.chmod(self.config_dir, 0o777)
        self.asn = asn
        self.router_id = router_id
        self.peers = {}
        self.routes = []
        self.policies = {}
        self.log_level = log_level
        super(BGPContainer, self).__init__(name, ctn_image_name)

    def start_goplane(self):
        name = '{0}/start_goplane.sh'.format(self.config_dir)
        with open(name, 'w') as f:
            f.write('''#!/bin/bash
goplane -f {0}/goplane.conf -l {1} -p > {0}/goplane.log 2>&1
'''.format(self.SHARED_VOLUME, self.log_level))
        os.chmod(name, 0o755)
        self.local('{0}/start_goplane.sh'.format(self.SHARED_VOLUME), detach=True)

    def run(self):
        super(BGPContainer, self).run()

    def add_peer(self, peer, passwd='', evpn=False, is_rs_client=False,
                 policies=None, passive=False):

        neigh_addr = ''
        for me, you in itertools.product(self.ip_addrs, peer.ip_addrs):
            if me[2] == you[2]:
                neigh_addr = you[1]

        if neigh_addr == '':
            raise Exception('peer {0} seems not ip reachable'.format(peer))

        if not policies:
            policies = {}

        self.peers[peer] = {'neigh_addr': neigh_addr,
                            'passwd': passwd,
                            'evpn': evpn,
                            'is_rs_client': is_rs_client,
                            'policies': policies,
                            'passive' : passive,
                            'remote_as': peer.asn}
        self.create_config()

    def add_peer_loopback(self, peer, passwd='', evpn=False, is_rs_client=False,
                 policies=None, passive=False, RouteReflector=False):

        if not policies:
            policies = {}

        self.peers[peer] = {'neigh_addr': peer.router_id,
                            'passwd': passwd,
                            'evpn': evpn,
                            'is_rs_client': is_rs_client,
                            'policies': policies,
                            'passive' : passive,
                            'RouteReflector': RouteReflector}
        self.create_config()

    def add_peer_for_ibgp(self, peer, peer_address, passwd='', evpn=False,
                 is_rs_client=False, policies=None, passive=False, RouteReflector=False):

        if not policies:
            policies = {}

        self.peers[peer] = {'neigh_addr': peer_address,
                            'passwd': passwd,
                            'evpn': evpn,
                            'is_rs_client': is_rs_client,
                            'policies': policies,
                            'passive' : passive,
                            'remote_as': peer.asn}
        self.create_config()

    def del_peer(self, peer):
        del self.peers[peer]
        self.create_config()
        if self.is_running:
            self.reload_config()

    def create_gobgp_config(self):
        config = {'global': {'config': {'as': self.asn, 'router-id': self.router_id},
                             'use-multiple-paths': {'config': {'enabled': True}}}}
        for peer, info in self.peers.items():
            afi_safi_list = []
            version = netaddr.IPNetwork(info['neigh_addr']).version
            if version == 4:
                afi_safi_list.append({'config': {'afi-safi-name': 'ipv4-unicast'}})
            elif version == 6:
                afi_safi_list.append({'config': {'afi-safi-name': 'ipv6-unicast'}})
            else:
                Exception('invalid ip address version. {0}'.format(version))

            if self.asn == peer.asn and info['RouteReflector'] is False:
                peer_type = self.PEER_TYPE_INTERNAL
                n = {'config': {
                        'peer-type': peer_type,
                        'neighbor-address': info['neigh_addr'],
                        'peer-as': peer.asn,
                        'local-as': self.asn,
                     },
                     'afi-safis': afi_safi_list,
                  }
            elif self.asn == peer.asn and info['RouteReflector'] is True:
                peer_type = self.PEER_TYPE_INTERNAL
                n = {'config': {
                        'peer-type': peer_type,
                        'neighbor-address': info['neigh_addr'],
                        'peer-as': peer.asn,
                        'local-as': self.asn,
                     },
                     'route-reflector': {
                        'config': {
                           'route-reflector-client': True,
                           'route-reflector-cluster-id': self.router_id,
                        }
                     },
                     'afi-safis': afi_safi_list,
                  }
            else:
                peer_type = self.PEER_TYPE_EXTERNAL
                n = {'config': {
                        'peer-type': peer_type,
                        'neighbor-address': info['neigh_addr'],
                        'peer-as': peer.asn,
                        'local-as': self.asn,
                     },
                     'afi-safis': afi_safi_list,
                     'ebgp-multihop': {
                        'config': {
                           'enabled': True,
                           'multihop-ttl': 3
                        }
                     },
                     'as-path-options': {
                        'config': {
                           'allow-own-as': 65000,
                           'replace-peer-as': True
                        }
                     }
                  }

            if len(info['passwd']) > 0:
                n['config']['auth-password'] = info['passwd']

            if info['evpn']:
                afi_safi_list.append({'config': {'afi-safi-name': 'l2vpn-evpn'}})

            if info['passive']:
                n['transport'] = {'config': {'passive-mode':True}}

            if info['is_rs_client']:
                n['route-server'] = {'config': {'route-server-client': True}}

            if 'neighbors' not in config:
                config['neighbors'] = []

            config['neighbors'].append(n)

        return config

    def create_config(self):
        dplane_config = {'type': 'netlink'}
        config = {'dataplane': dplane_config}
        bgp_config = self.create_gobgp_config()
        config['bgp'] = bgp_config

        with open('{0}/goplane.conf'.format(self.config_dir), 'w') as f:
            f.write(toml.dumps(config))

    def reload_config(self):
        raise Exception('implement reload_config() method')


class GoPlaneContainer(BGPContainer):

    PEER_TYPE_INTERNAL = 'internal'
    PEER_TYPE_EXTERNAL = 'external'
    SHARED_VOLUME = '/root/shared_volume'

    def __init__(self, name, asn, router_id, ctn_image_name='ttsubo/goplane',
                 log_level='debug', bgp_remote=False):
        super(GoPlaneContainer, self).__init__(name, asn, router_id,
                                             ctn_image_name)
        self.shared_volumes.append((self.config_dir, self.SHARED_VOLUME))
        self.vns = []
        self.log_level = log_level
        self.bgp_remote = bgp_remote

    def start_goplane(self):
        if self.bgp_remote:
            name = '{0}/start_gobgp.sh'.format(self.config_dir)
            with open(name, 'w') as f:
                f.write('''#!/bin/bash
    gobgpd -f {0}/gobgpd.conf -l {1} -p > {0}/gobgpd.log 2>&1
    '''.format(self.SHARED_VOLUME, self.log_level))
            os.chmod(name, 0o755)
            self.local('{0}/start_gobgp.sh'.format(self.SHARED_VOLUME), detach=True)

            time.sleep(1)

        name = '{0}/start_goplane.sh'.format(self.config_dir)
        with open(name, 'w') as f:
            f.write('''#!/bin/bash
goplane -f {0}/goplane.conf -l {1} -p > {0}/goplane.log 2>&1
'''.format(self.SHARED_VOLUME, self.log_level))
        os.chmod(name, 0o755)
        print(name, self.SHARED_VOLUME)
        self.local('{0}/start_goplane.sh'.format(self.SHARED_VOLUME), detach=True)


    def run(self):
        super(GoPlaneContainer, self).run()
        return self.WAIT_FOR_BOOT

    def create_goplane_config(self):
        dplane_config = {'type': 'netlink', 'virtual-network-list': []}
        for info in self.vns:
            dplane_config['virtual-network-list'].append({'rd': '{0}:{1}'.format(self.asn, info['vni']),
                                                          'vni': info['vni'],
                                                          'vxlan-port': info['vxlan_port'],
                                                          'vtep-interface': info['vtep'],
                                                          'etag': info['color'],
                                                          'sniff-interfaces': info['member'],
                                                          'member-interfaces': info['member']})

        config = {'dataplane': dplane_config}
        bgp_config = self.create_gobgp_config()
        config['bgp'] = bgp_config

        with open('{0}/goplane.conf'.format(self.config_dir), 'w') as f:
            f.write(toml.dumps(config))

    def create_config(self):
        self.create_goplane_config()

    def reload_config(self):
        self.local('/usr/bin/pkill goplane -SIGHUP')

    def add_vn(self, vni, vtep, color, member, vxlan_port=8472):
        self.vns.append({'vni':vni, 'vtep':vtep, 'vxlan_port':vxlan_port,
                         'color':color, 'member':member})


class QuaggaBGPContainer(BGPContainer):

    WAIT_FOR_BOOT = 1
    SHARED_VOLUME = '/etc/quagga'

    def __init__(self, name, asn, router_id, ctn_image_name='osrg/quagga', bgpd_config=None, zebra=False):
        super(QuaggaBGPContainer, self).__init__(name, asn, router_id,
                                                 ctn_image_name)
        self.shared_volumes.append((self.config_dir, self.SHARED_VOLUME))
        self.zebra = zebra

        # bgp_config is equivalent to config.BgpConfigSet structure
        # Example:
        # bgpd_config = {
        #     'global': {
        #         'confederation': {
        #             'identifier': 10,
        #             'peers': [65001],
        #         },
        #     },
        # }
        self.bgpd_config = bgpd_config or {}
        self.config_dir = "{0}/{1}".format(TEST_BASE_DIR, name)

    def _get_enabled_daemons(self):
        daemons = ['bgpd']
        if self.zebra:
            daemons.append('zebra')
        return daemons

    def _is_running(self):
        def f(d):
            return local(
                'vtysh -d {0} -c "show version"'
                ' > /dev/null 2>&1; echo $?'.format(d), capture=True) == '0'

        return all([f(d) for d in self._get_enabled_daemons()])

    def run(self):
        super(QuaggaBGPContainer, self).run()
        return self.WAIT_FOR_BOOT

    def get_global_rib(self, prefix='', rf='ipv4'):
        rib = []
        if prefix != '':
            return self.get_global_rib_with_prefix(prefix, rf)

        out = self.vtysh('show bgp {0} unicast'.format(rf), config=False)
        if out.startswith('No BGP network exists'):
            return rib

        for line in out.split('\n')[6:-2]:
            line = line[3:]

            p = line.split()[0]
            if '/' not in p:
                continue

            rib.extend(self.get_global_rib_with_prefix(p, rf))

        return rib

    def get_global_rib_with_prefix(self, prefix, rf):
        rib = []

        lines = [line.strip() for line in self.vtysh('show bgp {0} unicast {1}'.format(rf, prefix), config=False).split('\n')]

        if lines[0] == '% Network not in table':
            return rib

        lines = lines[2:]

        if lines[0].startswith('Not advertised'):
            lines.pop(0)  # another useless line
        elif lines[0].startswith('Advertised to non peer-group peers:'):
            lines = lines[2:]  # other useless lines
        else:
            raise Exception('unknown output format {0}'.format(lines))

        while len(lines) > 0:
            if lines[0] == 'Local':
                aspath = []
            else:
                aspath = [int(re.sub('\D', '', asn)) for asn in lines[0].split()]

            nexthop = lines[1].split()[0].strip()
            info = [s.strip(',') for s in lines[2].split()]
            attrs = []
            ibgp = False
            best = False
            if 'metric' in info:
                med = info[info.index('metric') + 1]
                attrs.append({'type': BGP_ATTR_TYPE_MULTI_EXIT_DISC, 'metric': int(med)})
            if 'localpref' in info:
                localpref = info[info.index('localpref') + 1]
                attrs.append({'type': BGP_ATTR_TYPE_LOCAL_PREF, 'value': int(localpref)})
            if 'internal' in info:
                ibgp = True
            if 'best' in info:
                best = True

            rib.append({'prefix': prefix, 'nexthop': nexthop,
                        'aspath': aspath, 'attrs': attrs, 'ibgp': ibgp, 'best': best})

            lines = lines[5:]

        return rib

    def get_neighbor_state(self, peer):
        if peer not in self.peers:
            raise Exception('not found peer {0}'.format(peer.router_id))

        neigh_addr = self.peers[peer]['neigh_addr'].split('/')[0]

        info = [l.strip() for l in self.vtysh('show bgp neighbors {0}'.format(neigh_addr), config=False).split('\n')]

        if not info[0].startswith('BGP neighbor is'):
            raise Exception('unknown format')

        idx1 = info[0].index('BGP neighbor is ')
        idx2 = info[0].index(',')
        n_addr = info[0][idx1 + len('BGP neighbor is '):idx2]
        if n_addr == neigh_addr:
            idx1 = info[2].index('= ')
            state = info[2][idx1 + len('= '):]
            if state.startswith('Idle'):
                return BGP_FSM_IDLE
            elif state.startswith('Active'):
                return BGP_FSM_ACTIVE
            elif state.startswith('Established'):
                return BGP_FSM_ESTABLISHED
            else:
                return state

        raise Exception('not found peer {0}'.format(peer.router_id))

    def send_route_refresh(self):
        self.vtysh('clear ip bgp * soft', config=False)

    def create_config(self):
        self._create_config_bgp()
        if self.zebra:
            self._create_config_zebra()

    def _create_config_bgp(self):

        c = CmdBuffer()
        c << 'hostname bgpd'
        c << 'password zebra'
        c << 'router bgp {0}'.format(self.asn)
        c << 'bgp router-id {0}'.format(self.router_id)

        if 'global' in self.bgpd_config:
            if 'confederation' in self.bgpd_config['global']:
                conf = self.bgpd_config['global']['confederation']['config']
                c << 'bgp confederation identifier {0}'.format(conf['identifier'])
                c << 'bgp confederation peers {0}'.format(' '.join([str(i) for i in conf['member-as-list']]))

        version = 4
        for peer, info in self.peers.items():
            version = netaddr.IPNetwork(info['neigh_addr']).version
            n_addr = info['neigh_addr'].split('/')[0]
            if version == 6:
                c << 'no bgp default ipv4-unicast'
            c << 'neighbor {0} remote-as {1}'.format(n_addr, info['remote_as'])
            # For rapid convergence
            c << 'neighbor {0} advertisement-interval 1'.format(n_addr)
            if info['is_rs_client']:
                c << 'neighbor {0} route-server-client'.format(n_addr)
            for typ, p in info['policies'].items():
                c << 'neighbor {0} route-map {1} {2}'.format(n_addr, p['name'],
                                                             typ)
            if info['passwd']:
                c << 'neighbor {0} password {1}'.format(n_addr, info['passwd'])
            if info['passive']:
                c << 'neighbor {0} passive'.format(n_addr)
            if version == 6:
                c << 'address-family ipv6 unicast'
                c << 'neighbor {0} activate'.format(n_addr)
                c << 'exit-address-family'

        if self.zebra:
            if version == 6:
                c << 'address-family ipv6 unicast'
#                c << 'redistribute connected'
                c << 'exit-address-family'
#            else:
#                c << 'redistribute connected'

        for name, policy in self.policies.items():
            c << 'access-list {0} {1} {2}'.format(name, policy['type'],
                                                  policy['match'])
            c << 'route-map {0} permit 10'.format(name)
            c << 'match ip address {0}'.format(name)
            c << 'set metric {0}'.format(policy['med'])

        c << 'debug bgp as4'
        c << 'debug bgp fsm'
        c << 'debug bgp updates'
        c << 'debug bgp events'
        c << 'log file {0}/bgpd.log'.format(self.SHARED_VOLUME)

        with open('{0}/bgpd.conf'.format(self.config_dir), 'w') as f:
            print('[{0}\'s new bgpd.conf]'.format(self.name))
            print(indent(str(c)))
            f.writelines(str(c))

    def _create_config_zebra(self):
        c = CmdBuffer()
        c << 'hostname zebra'
        c << 'password zebra'
        c << 'log file {0}/zebra.log'.format(self.SHARED_VOLUME)
        c << 'debug zebra packet'
        c << 'debug zebra kernel'
        c << 'debug zebra rib'
        c << 'ipv6 forwarding'
        c << ''

        with open('{0}/zebra.conf'.format(self.config_dir), 'w') as f:
            print('[{0}\'s new zebra.conf]'.format(self.name))
            c = str(c).strip()
            print(indent(c))
            f.writelines(c)

    def vtysh(self, cmd, config=True):
        if not isinstance(cmd, list):
            cmd = [cmd]
        cmd = ' '.join("-c '{0}'".format(c) for c in cmd)
        if config:
            return self.local("vtysh -d bgpd -c 'enable' -c 'conf t' -c 'router bgp {0}' {1}".format(self.asn, cmd), capture=True)
        else:
            return self.local("vtysh -d bgpd {0}".format(cmd), capture=True)

    def reload_config(self):
        for daemon in self._get_enabled_daemons():
            self.local('pkill -SIGHUP {0}'.format(daemon), capture=True)
        self._wait_for_boot()

    def _vtysh_add_route_map(self, path):
        supported_attributes = (
            'next-hop',
            'as-path',
            'community',
            'med',
            'local-pref',
            'extended-community',
        )
        if not any([path[k] for k in supported_attributes]):
            return ''

        c = CmdBuffer(' ')
        route_map_name = 'RM-{0}'.format(path['prefix'])
        c << "vtysh -c 'configure terminal'"
        c << "-c 'route-map {0} permit 10'".format(route_map_name)
        if path['next-hop']:
            if path['rf'] == 'ipv4':
                c << "-c 'set ip next-hop {0}'".format(path['next-hop'])
            elif path['rf'] == 'ipv6':
                c << "-c 'set ipv6 next-hop {0}'".format(path['next-hop'])
            else:
                raise ValueError('Unsupported address family: {0}'.format(path['rf']))
        if path['as-path']:
            as_path = ' '.join([str(n) for n in path['as-path']])
            c << "-c 'set as-path prepend {0}'".format(as_path)
        if path['community']:
            comm = ' '.join(path['community'])
            c << "-c 'set community {0}'".format(comm)
        if path['med']:
            c << "-c 'set metric {0}'".format(path['med'])
        if path['local-pref']:
            c << "-c 'set local-preference {0}'".format(path['local-pref'])
        if path['extended-community']:
            # Note: Currently only RT is supported.
            extcomm = ' '.join(path['extended-community'])
            c << "-c 'set extcommunity rt {0}'".format(extcomm)
        self.local(str(c), capture=True)

        return route_map_name

    def add_route(self, route, rf='ipv4', attribute=None, aspath=None,
                  community=None, med=None, extendedcommunity=None,
                  nexthop=None, matchs=None, thens=None,
                  local_pref=None, identifier=None, reload_config=False):
        if not self._is_running():
            raise RuntimeError('Quagga/Zebra is not yet running')

        if rf not in ('ipv4', 'ipv6'):
            raise ValueError('Unsupported address family: {0}'.format(rf))

        self.routes.setdefault(route, [])
        path = {
            'prefix': route,
            'rf': rf,
            'next-hop': nexthop,
            'as-path': aspath,
            'community': community,
            'med': med,
            'local-pref': local_pref,
            'extended-community': extendedcommunity,
            # Note: The following settings are not yet supported on this
            # implementation.
            'attr': None,
            'identifier': None,
            'matchs': None,
            'thens': None,
        }

        # Prepare route-map before adding prefix
        route_map_name = self._vtysh_add_route_map(path)
        path['route_map'] = route_map_name

        c = CmdBuffer(' ')
        c << "vtysh -c 'configure terminal'"
        c << "-c 'router bgp {0}'".format(self.asn)
        if rf == 'ipv6':
            c << "-c 'address-family ipv6'"
        if route_map_name:
            c << "-c 'network {0} route-map {1}'".format(route, route_map_name)
        else:
            c << "-c 'network {0}'".format(route)
        self.local(str(c), capture=True)

        self.routes[route].append(path)

    def _vtysh_del_route_map(self, path):
        route_map_name = path.get('route_map', '')
        if not route_map_name:
            return

        c = CmdBuffer(' ')
        c << "vtysh -c 'configure terminal'"
        c << "-c 'no route-map {0}'".format(route_map_name)
        self.local(str(c), capture=True)

    def del_route(self, route, identifier=None, reload_config=False):
        if not self._is_running():
            raise RuntimeError('Quagga/Zebra is not yet running')

        path = None
        new_paths = []
        for p in self.routes.get(route, []):
            if p['identifier'] != identifier:
                new_paths.append(p)
            else:
                path = p
        if not path:
            return

        rf = path['rf']
        c = CmdBuffer(' ')
        c << "vtysh -c 'configure terminal'"
        c << "-c 'router bgp {0}'".format(self.asn)
        c << "-c 'address-family {0} unicast'".format(rf)
        c << "-c 'no network {0}'".format(route)
        self.local(str(c), capture=True)

        # Delete route-map after deleting prefix
        self._vtysh_del_route_map(path)

        self.routes[route] = new_paths


class FrroutingBGPContainer(BGPContainer):

    WAIT_FOR_BOOT = 1
    SHARED_VOLUME = '/etc/frr'

    def __init__(self, name, asn, router_id, ctn_image_name='ttsubo/frrouting', bgpd_config=None):
        config_dir = "{0}/{1}".format('/tmp/Frrouting', name)
        if os.path.exists(config_dir) is False:
            shutil.copytree('./conf/Frrouting', config_dir)
        super(FrroutingBGPContainer, self).__init__(name, asn, router_id,
                                                 ctn_image_name, config_dir=config_dir)
        self.bgpd_config = bgpd_config or {}

    def start_goplane(self):
        self.local("vtysh -b /etc/frr/frr.conf", detach=True)

    def run(self):
        super(FrroutingBGPContainer, self).run()
        return self.WAIT_FOR_BOOT

    def create_config(self):
        self._create_config_bgp()
        uid = 104
        gid = 108
        os.chown("{0}/frr.conf".format(self.config_dir), uid, gid)

    def _create_config_bgp(self):
        c = CmdBuffer()
        c << 'router bgp {0}'.format(self.asn)
        c << 'bgp router-id {0}'.format(self.router_id)

        if 'global' in self.bgpd_config:
            if 'confederation' in self.bgpd_config['global']:
                conf = self.bgpd_config['global']['confederation']['config']
                c << 'bgp confederation identifier {0}'.format(conf['identifier'])
                c << 'bgp confederation peers {0}'.format(' '.join([str(i) for i in conf['member-as-list']]))

        version = 4
        for peer, info in self.peers.items():
            version = netaddr.IPNetwork(info['neigh_addr']).version
            n_addr = info['neigh_addr'].split('/')[0]
            if version == 6:
                c << 'no bgp default ipv4-unicast'
            c << 'neighbor {0} remote-as {1}'.format(n_addr, info['remote_as'])
            # For rapid convergence
            c << 'neighbor {0} advertisement-interval 1'.format(n_addr)
            if info['is_rs_client']:
                c << 'neighbor {0} route-server-client'.format(n_addr)
            for typ, p in info['policies'].items():
                c << 'neighbor {0} route-map {1} {2}'.format(n_addr, p['name'],
                                                             typ)
            if info['passwd']:
                c << 'neighbor {0} password {1}'.format(n_addr, info['passwd'])
            if info['passive']:
                c << 'neighbor {0} passive'.format(n_addr)
            if version == 6:
                c << 'address-family ipv6 unicast'
                c << 'neighbor {0} activate'.format(n_addr)
                c << 'redistribute connected'
                c << 'exit-address-family'
            else:
                c << 'redistribute connected'
                c << 'redistribute static'
                if self.asn == info['remote_as']:
                    c << 'neighbor {0} next-hop-self'.format(n_addr)


        for name, policy in self.policies.items():
            c << 'access-list {0} {1} {2}'.format(name, policy['type'],
                                                  policy['match'])
            c << 'route-map {0} permit 10'.format(name)
            c << 'match ip address {0}'.format(name)
            c << 'set metric {0}'.format(policy['med'])

        with open('{0}/frr.conf'.format(self.config_dir), 'w') as f:
            print('[{0}\'s new frr.conf]'.format(self.name))
            print(indent(str(c)))
            f.writelines(str(c))

