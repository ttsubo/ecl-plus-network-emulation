// Go NETCONF Client - Juniper Example (show system information)
//
// Copyright (c) 2013-2018, Juniper Networks, Inc. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
package main
import (
	"bufio"
	"encoding/xml"
	"flag"
	"fmt"
	"log"
	"os"
	"syscall"
	"github.com/Juniper/go-netconf/netconf"
	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/terminal"
)
var (
	host         = flag.String("host", "127.0.0.1", "Hostname")
	username     = flag.String("username", "", "Username")
	key          = flag.String("key", os.Getenv("HOME")+"/.ssh/id_rsa", "SSH private key file")
	passphrase   = flag.String("passphrase", "", "SSH private key passphrase (cleartext)")
	nopassphrase = flag.Bool("nopassphrase", false, "SSH private key does not contain a passphrase")
	pubkey       = flag.Bool("pubkey", false, "Use SSH public key authentication")
	agent        = flag.Bool("agent", false, "Use SSH agent for public key authentication")
)
// VRRPInformation provides a representation of the vrrp-information container
type VRRPInformation struct {
	XMLName    xml.Name `xml:"vrrp-information"`
	Interfaces []struct {
		IfName string `xml:"interface"`
		Vrid   int    `xml:"group"`
		State  string `xml:"vrrp-state"`
	} `xml:"vrrp-interface"`
}
// BuildConfig captures information from the console to build a SSH Client Config
func BuildConfig() *ssh.ClientConfig {
	var config *ssh.ClientConfig
	var pass string
	if *pubkey {
		if *agent {
			var err error
			config, err = netconf.SSHConfigPubKeyAgent(*username)
			if err != nil {
				log.Fatal(err)
			}
		} else {
			if *nopassphrase {
				pass = "\n"
			} else {
				if *passphrase != "" {
					pass = *passphrase
				} else {
					var readpass []byte
					var err error
					fmt.Printf("Enter Passphrase for %s: ", *key)
					readpass, err = terminal.ReadPassword(syscall.Stdin)
					if err != nil {
						log.Fatal(err)
					}
					pass = string(readpass)
					fmt.Println()
				}
			}
			var err error
			config, err = netconf.SSHConfigPubKeyFile(*username, *key, pass)
			if err != nil {
				log.Fatal(err)
			}
		}
	} else {
		fmt.Printf("Enter Password: ")
		bytePassword, err := terminal.ReadPassword(syscall.Stdin)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println()
		config = netconf.SSHConfigPassword(*username, string(bytePassword))
	}
	return config
}
func main() {
	flag.Parse()
	if *username == "" {
		fmt.Printf("Enter a valid username: ")
		r := bufio.NewScanner(os.Stdin)
		r.Scan()
		*username = r.Text()
	}
	config := BuildConfig()
	s, err := netconf.DialSSH(*host, config)
	if err != nil {
		log.Fatal(err)
	}
	defer s.Close()
	rpcReply, err := s.Exec(netconf.RawMethod("<get-vrrp-information/>"))
	if err != nil {
		panic(err)
	}
	vrrpInfo := VRRPInformation{}
	err = xml.Unmarshal([]byte(rpcReply.Data), &vrrpInfo)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("VRRP Status Reply: %s", rpcReply.Data)
}
