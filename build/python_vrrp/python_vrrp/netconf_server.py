import sys
import logging
import random
import sys
import logging
import random
import socket
import netaddr
import threading
import eventlet
import vrrp_main
from binascii import hexlify
from collections import defaultdict
from io import StringIO
from lxml import etree
import time
eventlet.monkey_patch()

import jinja2
import paramiko
from lxml import etree
from paramiko.py3compat import u
from pkg_resources import resource_string
from plumbum import cli

logger = logging.getLogger("nc_mock")
logging.basicConfig(
    level="INFO", format="%(asctime)s [%(levelname)s] %(message)s")

DELIM = ']]>]]>'
SESSION_ID = 98956
BUFF_SIZE = 4096
MGMT_PREFIX = 'netconf_mock_command:'
WAIT_EVENT_TIMEOUT = 30


# message was captured (mx really does send the 'zombie comment')
HELLO_REPLAY = '''\
<!-- No zombies were killed during the creation of this user interface -->
<!-- user root, class super-user -->
<hello xmlns="urn:ietf:params:xml:ns:netconf:base:1.0">
  <capabilities>
    <capability>urn:ietf:params:netconf:base:1.0</capability>
    <capability>urn:ietf:params:netconf:capability:candidate:1.0</capability>
    <capability>urn:ietf:params:netconf:capability:confirmed-commit:1.0</capability>
    <capability>urn:ietf:params:netconf:capability:validate:1.0</capability>
    <capability>urn:ietf:params:netconf:capability:url:1.0?scheme=http,ftp,file</capability>
    <capability>urn:ietf:params:xml:ns:netconf:base:1.0</capability>
    <capability>urn:ietf:params:xml:ns:netconf:capability:candidate:1.0</capability>
    <capability>urn:ietf:params:xml:ns:netconf:capability:confirmed-commit:1.0</capability>
    <capability>urn:ietf:params:xml:ns:netconf:capability:validate:1.0</capability>
    <capability>urn:ietf:params:xml:ns:netconf:capability:url:1.0?protocol=http,ftp,file</capability>
    <capability>http://xml.juniper.net/netconf/junos/1.0</capability>
    <capability>http://xml.juniper.net/dmi/system/1.0</capability>
  </capabilities>
  <session-id>{}</session-id>
</hello>
{}
'''.format(SESSION_ID, DELIM)

OK_REPLY = '<ok/>'

VRRP_REPLY_TEMPLATE = jinja2.Template('''
<vrrp-information style="brief">
{% for session in sessions %}
<vrrp-interface>
  <interface>{{ session.interface }}</interface>
  <interface-state>up</interface-state>
  <group>{{ session.vrid }}</group>
  <vrrp-state>{{ session.state }}</vrrp-state>
  <vrrp-mode>Active</vrrp-mode>
  <timer-name>A</timer-name>
  <timer-value>2.071</timer-value>
  <local-interface-address>{{ session.rip }}</local-interface-address>
  <virtual-ip-address>{{ session.vip }}</virtual-ip-address>
</vrrp-interface>
{% endfor %}
</vrrp-information>
''')


def make_rpc_replay(uuid, xml):
    return '''\
<rpc-reply
    xmlns="urn:ietf:params:xml:ns:netconf:base:1.0"
    xmlns:junos="http://xml.juniper.net/junos/14.2R3/junos"
    xmlns:nc="urn:ietf:params:xml:ns:netconf:base:1.0"
    message-id="{}">
{}
</rpc-reply>
{}
'''.format(uuid, xml, DELIM)


def reply_dict_default_entry():
    return {"default": OK_REPLY, "sequence": [], "close_session": False}


class Singleton(object):
    def __new__(cls, *args, **kargs):
        if not hasattr(cls, "_instance"):
            cls._instance = super(Singleton, cls).__new__(cls)
            cls.once = True
        return cls._instance


class NetconfMockServer(paramiko.ServerInterface, Singleton):
    userame = 'user'

    @classmethod
    def reset(cls):
        cls.reply_dict = defaultdict(reply_dict_default_entry)
        cls.reply_dict['close-session']['close_session'] = True
        cls.reply_dict['command']['commands'] = \
            defaultdict(reply_dict_default_entry)
        cls.request_log = []
        cls.password = App.password

    def __init__(self, ident=None):
        self.ident = ident
        self.event = threading.Event()
        self.channel = None
        self.data = ""

    def refresh_status(self, ifname, vrid, status, rip, vip):
        vrrp_session = [{'interface': ifname,
                         'vrid': vrid,
                         'state': status,
                         'rip': rip,
                         'vip': vip}]
        self.reply_dict['get-vrrp-information'][
            'default'] = VRRP_REPLY_TEMPLATE.render(sessions=vrrp_session)


    def check_channel_request(self, kind, chanid):
        if kind == 'session':
            return paramiko.OPEN_SUCCEEDED
        return paramiko.OPEN_FAILED_ADMINISTRATIVELY_PROHIBITED

    def check_auth_password(self, username, password):
        if (username == self.username) and (password == self.password):
            return paramiko.AUTH_SUCCESSFUL
        return paramiko.AUTH_FAILED

    def get_allowed_auths(self, username):
        return 'password'

    def check_channel_exec_request(self, channel, command):
        self.channel = channel
        self.event.set()
        return True

    def check_channel_subsystem_request(self, channel, name):
        self.channel = channel
        self.event.set()
        return True

    def receive_data(self):
        idx = self.data.find(DELIM)
        while idx < 0:
            try:
                new_data = self.channel.recv(BUFF_SIZE)
            except Exception as e:
                logger.error(e)
                return None
            if new_data:
                # logger.debug('received data chunk:\n%s\n', new_data)
                self.data += new_data.decode()
                idx = self.data.find(DELIM)
            if idx < 0:
                time.sleep(0.1)
        data = self.data[:idx].strip()
        self.data = self.data[idx + len(DELIM):]
        return data

    def process_request(self, xml_data):
        result = False
        request_info = dict(raw=xml_data)
        xml_data = xml_data.encode('utf-8')
        root = etree.fromstring(xml_data)
        uuid = root.attrib.get('message-id', None)
        request_type = etree.QName(root[0]).localname
        request_info["type"] = request_type
        logger.debug('%s RPC received (message-id: %s rpc: %s)', self.ident, uuid, request_type)


        # We should not response to capabilities message
        if request_type == "capabilities":
            return

        reply_dict_entry = self.reply_dict[request_type]
        if request_type == "command":
            cmd = root[0].text
            request_info["cmd"] = cmd
            if ("commands" in reply_dict_entry
                    and cmd in reply_dict_entry["commands"]):
                reply_dict_entry = reply_dict_entry["commands"][cmd]

        if reply_dict_entry["sequence"]:
            reply = reply_dict_entry["sequence"].pop()
        else:
            reply = reply_dict_entry["default"]
            if callable(reply):
                reply = reply(xml_data)

        reply = make_rpc_replay(uuid, reply)
        logger.debug("Sending reply:\n%s\n", reply)
        self.channel.sendall(reply)

        if request_type == "load-configuration":
            vrrp_params, result = self.parse_vrrp_params(xml_data)
        if result:
            self.start_vrrp_instance(**vrrp_params)

    def parse_vrrp_params(self, xml_data):
        vrid = None
        root = etree.fromstring(xml_data)
        config_data = root.find('.//{*}configuration-text').text
        logger.info("config_data=[%s]"%config_data)
        config_list = config_data.split('\n')
        for conf in config_list:
            if conf.lstrip(' ').startswith("vrrp-group"):
                vrid = conf.lstrip(" ").split(" ")[1]
            elif conf.lstrip(' ').startswith("unit"):
                vlan = conf.lstrip(" ").split(" ")[1]
            elif conf.lstrip(' ').startswith("address"):
                primary_ip = conf.lstrip(" ").split(" ")[1]
            elif conf.lstrip(' ').startswith("virtual-address"):
                virtual_ip_address = conf.lstrip(" ").split(" ")[1].rstrip(';')
            elif conf.lstrip(' ').startswith("priority"):
                priority = conf.lstrip(" ").split(" ")[1].rstrip(';')
        if vlan == '0':
            ifname = config_list[2].lstrip(" ").split(" ")[0]
        else:
            ifname = config_list[2].lstrip(" ").split(" ")[0] + '.' + str(vlan)
        primary_ip_address = str(netaddr.IPNetwork(primary_ip).ip)
        mask = str(netaddr.IPNetwork(primary_ip).prefixlen)
        if vrid is None:
            return {}, False
        vrrp_params = {
            "vrid": vrid,
            "ifname": ifname,
            "primary_ip_address": primary_ip_address,
            "virtual_ip_address": virtual_ip_address,
            "mask": mask,
            "priority": priority,
        }
        return vrrp_params, True

    def start_vrrp_instance(self, vrid, ifname, primary_ip_address, mask, virtual_ip_address, priority):
        logger.info("vrrp params: vrid=[{0}], ifname=[{1}], primary_ip_address=[{2}], mask=[{3}], virtual_ip_address=[{4}], priority=[{5}]".format(vrid, ifname, primary_ip_address, mask, virtual_ip_address, priority))
        if primary_ip_address == '192.168.3.2':
            virtual_mac_address = 'aa:aa:aa:aa:aa:02'
        elif primary_ip_address == '192.168.3.3':
            virtual_mac_address = 'aa:aa:aa:aa:aa:03'
        else:
            virtual_mac_address = 'aa:aa:aa:aa:aa:aa'
        if self.once:
            eventlet.spawn(vrrp_main.start_vrrp,
                           self,
                           vrid,
                           ifname,
                           primary_ip_address,
                           virtual_ip_address,
                           mask,
                           virtual_mac_address,
                           priority)
            self.once = False

    def handle_session(self):
        self.channel.sendall(HELLO_REPLAY)
        while True:
            xml_data = self.receive_data()
            if xml_data is None:
                break
            logger.debug("received data:\n%s\n", repr(xml_data))
            self.process_request(xml_data)


class App(cli.Application):
    port = 830
    http_port = 8088
    http_host = "127.0.0.1"
    username = 'user'
    password = '1234'

    @cli.switch(['-P'], argtype=int)
    def port_switch(self, port):
        """server port"""
        App.port = port

    @cli.switch(['-u'], argtype=str)
    def user_switch(self, username):
        """clients username"""
        App.username = username

    @cli.switch(['-p'], argtype=str)
    def pass_switch(self, password):
        """clients password"""
        App.password = password

    def handle_connection(self, transport, ident):
        self.server = NetconfMockServer()
        try:
            self.server.ident = ident
            self.server.username = self.username
            self.server.password = self.password
            try:
                transport.start_server(server=self.server)
            except paramiko.SSHException:
                logger.exception('%s SSH negotiation failed.', ident)
                return

            # wait for auth
            chan = transport.accept(20)
            if chan is None:
                logger.error('%s No channel.', ident)
                return
            logger.info('%s Authenticated!', ident)

            # if no action is received within timeout, close connection
            self.server.event.wait(WAIT_EVENT_TIMEOUT)
            if not self.server.event.is_set():
                logger.error('%s Client did not execute xml-mode command',
                             ident)
                return
            self.server.handle_session()

        except Exception as e:
            logger.exception("Unhandled exception, closing transport")
            try:
                transport.close()
            except Exception:
                logger.exception("transport.close failed")

    def run_server(self):
        host_key = paramiko.RSAKey(filename="netconf_rsa.key")
        logger.debug('Read key: %s', u(hexlify(host_key.get_fingerprint())))

        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            sock.bind(('', self.port))
        except Exception:
            logger.error('Bind failed')
            return

        logger.info('Port: {}'.format(self.port))
        logger.info('Username: {}'.format(self.username))
        logger.info('Password: {}'.format(self.password))
        logger.info('Listening for connections ...')
        while True:
            try:
                sock.listen(100)
                client, addr = sock.accept()
            except Exception:
                logger.exception('*** Listen/accept failed')
                continue

            logger.debug('Got a connection! {}'.format(addr))

            t = paramiko.Transport(client)
            t.add_server_key(host_key)
            eventlet.spawn(self.handle_connection, t, str(addr))

    def main(self):
        try:
            server_thred = eventlet.spawn(self.run_server)
            server_thred.wait()
        except KeyboardInterrupt:
            pass


NetconfMockServer.reset()


if __name__ == "__main__":
    App.run()
