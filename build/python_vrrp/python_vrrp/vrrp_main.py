import os
import logging
import time
import eventlet
import contextlib
import greenlet
import socket
import struct
import binascii
import netaddr
import signal
from pyroute2 import IPRoute
from ryu.lib import hub
from ryu.lib.rpc import RPCError
from ryu.lib import mac as mac_lib
from ryu.lib.packet import arp
from ryu.lib.packet import ethernet
from ryu.lib.packet import packet
from ryu.lib.packet import vrrp
from ryu.ofproto import ether
from python_vrrp.base import api as vrrp_api
from python_vrrp.base import event as vrrp_event
from python_vrrp.base import utils
from python_vrrp.base import handler
from python_vrrp.app_manager import VrrpApp, AppManager
eventlet.monkey_patch()

LOG = logging.getLogger('VrrpMain')
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(filename)s:%(lineno)s:%(funcName)s:%(message)s',
                    level=logging.INFO)

_VRRP_VERSION_V3 = 3
_PREEMPT_DELAY = 10

def receive_signal(signum, stack):
    print("\nexit")
    ip = IPRoute()
    guest = ip.link_lookup(ifname=IFNAME)[0]
    for a in ip.get_addr(index=guest):
        if a.get_attrs('IFA_ADDRESS')[0] == VIRTUAL_IP_ADDRESS:
            ip.addr('del', index=guest, address=VIRTUAL_IP_ADDRESS, mask=int(MASK))
    os._exit(0)


class SampleVrrp(VrrpApp):
    def __init__(self, *args, **kwargs):
        super(SampleVrrp, self).__init__(*args, **kwargs)
        self.vrrp_thread = hub.spawn(self._start)
        self._arp_thread = None
        self.interface = vrrp_event.VRRPInterfaceNetworkDevice(
            VIRTUAL_MAC_ADDRESS, PRIMARY_IP_ADDRESS, None, IFNAME)
        self.garp_packets = [self._garp_packet(VIRTUAL_IP_ADDRESS)]
        self.monitor_name = "{0}-{1}-{2}".format("VRRPInterfaceMonitorNetworkDevice",str(self.interface),int(VRID))

    def _start(self):
        LOG.info("")
        LOG.info("////// 1. Create Vrrp Router  //////")
        self._configure_vrrp_router(_VRRP_VERSION_V3,
                                    int(PRIORITY),
                                    PRIMARY_IP_ADDRESS,
                                    VIRTUAL_IP_ADDRESS,
                                    IFNAME,
                                    int(VRID),
                                    _PREEMPT_DELAY)
    
    def _configure_vrrp_change(self, vrid, priority):
        instance_name = self._lookup_instance(vrid)
        if not instance_name:
            raise RPCError('vrid %d is not found' % (vrid))
        vrrp_api.vrrp_config_change(self, instance_name, priority=priority)

    def _shutdown_vrrp_router(self, vrid):
        instance_name = self._lookup_instance(vrid)
        if not instance_name:
            raise RPCError('vrid %d is not found' % (vrid))
        vrrp_api.vrrp_shutdown(self, instance_name)

    def _lookup_instance(self, vrid):
        for instance in vrrp_api.vrrp_list(self).instance_list:
            if vrid == instance.config.vrid:
                return instance.instance_name
        return None

    def _configure_vrrp_router(self, vrrp_version, vrrp_priority,
                               primary_ip_address, virtual_ip_address,
                               ifname, vrid, preempt_delay):
        interface = vrrp_event.VRRPInterfaceNetworkDevice(
            VIRTUAL_MAC_ADDRESS, primary_ip_address, None, ifname)

        ip_addresses = [virtual_ip_address]
        config = vrrp_event.VRRPConfig(
            version=vrrp_version, vrid=vrid, priority=vrrp_priority,
            ip_addresses=ip_addresses, preempt_delay=preempt_delay)
        config_result = vrrp_api.vrrp_config(self, interface, config)
        return config_result

    @handler.set_ev_cls(vrrp_event.EventVRRPStateChanged)
    def vrrp_state_changed_handler(self, ev):
        old_state = ev.old_state
        new_state = ev.new_state
        LOG.info("State Changed [%s] -> [%s]"% (old_state, new_state))

        if new_state == vrrp_event.VRRP_STATE_MASTER:
            if old_state == vrrp_event.VRRP_STATE_INITIALIZE:
                self._initialized_to_master()
            elif old_state == vrrp_event.VRRP_STATE_BACKUP:
                self._become_master()

        elif new_state == vrrp_event.VRRP_STATE_BACKUP:
            self._become_backup()
        elif new_state == vrrp_event.VRRP_STATE_INITIALIZE:
            if old_state is None:
                self._initialized()
            else:
                self._shutdowned()
        else:
            raise ValueError('invalid vrrp state %s' % new_state)

    def _transmit(self, data):
        vrrp_api.vrrp_transmit(self, self.monitor_name, data)

    def _initialized(self):
        self.logger.debug('initialized')

    def _initialized_to_master(self):
        self.logger.debug('initialized to master')
        self._master()

    def _become_master(self):
        self.logger.debug('become master')
        self._master()

    def _master(self):
        self.__is_master = True
        NETCONF_SERVER.refresh_status(IFNAME, VRID, "master", PRIMARY_IP_ADDRESS, VIRTUAL_IP_ADDRESS)
        self._enable_router()
        self._send_garp()

    def _become_backup(self):
        self.logger.debug('become backup')
        self.__is_master = False
        NETCONF_SERVER.refresh_status(IFNAME, VRID, "backup", PRIMARY_IP_ADDRESS, VIRTUAL_IP_ADDRESS)
        self._disable_router()

    def _shutdowned(self):
        # When VRRP functionality is disabled, what to do?
        #  should we also exit? or continue to route packets?
        self._disable_router()

    def _enable_router(self):
        self.logger.info('# add virtual_ip_address')
        ip = IPRoute()
        guest = ip.link_lookup(ifname=IFNAME)[0]
        ip.addr('add', index=guest, address=VIRTUAL_IP_ADDRESS, mask=int(MASK))

    def _disable_router(self):
        self.logger.info('# del virtual_ip_address')
        ip = IPRoute()
        guest = ip.link_lookup(ifname=IFNAME)[0]
        for a in ip.get_addr(index=guest):
            if a.get_attrs('IFA_ADDRESS')[0] == VIRTUAL_IP_ADDRESS:
                ip.addr('del', index=guest, address=VIRTUAL_IP_ADDRESS, mask=int(MASK))

    def _garp_packet(self, ip_address):
        # prepare garp packet
        src_mac = vrrp.vrrp_ipv4_src_mac_address(int(VRID))
        e = ethernet.ethernet(mac_lib.BROADCAST_STR, src_mac,
                              ether.ETH_TYPE_ARP)
        a = arp.arp_ip(arp.ARP_REQUEST, src_mac, ip_address,
                       mac_lib.DONTCARE_STR, ip_address)

        p = packet.Packet()
        p.add_protocol(e)
        utils.may_add_vlan(p, self.interface.vlan_id)
        p.add_protocol(a)
        p.serialize()
        return p

    def _send_garp(self):
        self.logger.debug('_send_garp')
        for garp_packet in self.garp_packets:
            self._transmit(garp_packet.data)


def start_vrrp(netconf_server, vrid, ifname, primary_ip, virtual_ip, mask, virtual_mac_address, priority):
    global NETCONF_SERVER, VRID, IFNAME, PRIMARY_IP_ADDRESS, VIRTUAL_IP_ADDRESS, MASK, VIRTUAL_MAC_ADDRESS, PRIORITY
    NETCONF_SERVER = netconf_server
    VRID = vrid
    IFNAME = ifname
    PRIMARY_IP_ADDRESS = primary_ip
    VIRTUAL_IP_ADDRESS = virtual_ip
    MASK = mask
    VIRTUAL_MAC_ADDRESS = virtual_mac_address
    PRIORITY = priority

    app_lists = ['vrrp_main.py']
    app_mgr = AppManager.get_instance()
    app_mgr.load_apps(app_lists)
    contexts = app_mgr.create_contexts()
    services = []
    services.extend(app_mgr.instantiate_apps(**contexts))
    def dummy_loop():
        while True:
            signal.signal(signal.SIGINT, receive_signal)
            time.sleep(1)
    opthread = eventlet.spawn(dummy_loop)
    try:
        hub.joinall(services)
    except KeyboardInterrupt:
        LOG.info("Keyboard Interrupt received. "
                     "Closing VRRP application manager...")
    finally:
        app_mgr.close()
