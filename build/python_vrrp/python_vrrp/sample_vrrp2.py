import logging
from ncclient.manager import connect_ssh

log = logging.getLogger(__name__)


CONFIG = '''
interfaces {
  eth0 {
    unit 0 {
      family {
        inet {
          address 192.168.3.3/24
            {
              vrrp-group 1 {
                virtual-address 192.168.3.100;
                priority 100;
              }
            }
        }
      }
    }
  }
}
'''


def execute_netconf():
    manager = connect_ssh(
                  host='127.0.0.1',
                  port='830',
                  username='user',
                  password='1234',
                  hostkey_verify=False,
                  device_params={"name": 'junos'}
              )
    manager.discard_changes()
    with manager.locked('candidate'):
        log.info("Locked candidate config")
        try:
            manager.load_configuration(target="candidate",
                                       action='merge',
                                       config=CONFIG,
                                       format='text')
            log.info("Netconf sent, commiting...")
            manager.commit()
        except Exception as exc:
            manager.discard_changes()
    manager.close_session()

if __name__ == "__main__":
    execute_netconf()
