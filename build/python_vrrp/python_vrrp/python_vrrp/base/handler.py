import inspect
import logging
import sys

LOG = logging.getLogger('python_vrrp.base.handler')

HANDSHAKE_DISPATCHER = "handshake"
CONFIG_DISPATCHER = "config"
MAIN_DISPATCHER = "main"
DEAD_DISPATCHER = "dead"


class _Caller(object):
    def __init__(self, dispatchers, ev_source):
        self.dispatchers = dispatchers
        self.ev_source = ev_source


# should be named something like 'observe_event'
def set_ev_cls(ev_cls, dispatchers=None):
    def _set_ev_cls_dec(handler):
        if 'callers' not in dir(handler):
            handler.callers = {}
        for e in _listify(ev_cls):
            handler.callers[e] = _Caller(_listify(dispatchers), e.__module__)
        return handler
    return _set_ev_cls_dec


def set_ev_handler(ev_cls, dispatchers=None):
    def _set_ev_cls_dec(handler):
        if 'callers' not in dir(handler):
            handler.callers = {}
        for e in _listify(ev_cls):
            handler.callers[e] = _Caller(_listify(dispatchers), None)
        return handler
    return _set_ev_cls_dec


def _has_caller(meth):
    return hasattr(meth, 'callers')


def _listify(may_list):
    if may_list is None:
        may_list = []
    if not isinstance(may_list, list):
        may_list = [may_list]
    return may_list


def register_instance(i):
    for _k, m in inspect.getmembers(i, inspect.ismethod):
        # LOG.debug('instance %s k %s m %s', i, _k, m)
        if _has_caller(m):
            for ev_cls, _ in m.callers.items():
                i.register_handler(ev_cls, m)


def _is_method(f):
    return inspect.isfunction(f) or inspect.ismethod(f)


def get_dependent_services(cls):
    services = []
    for _k, m in inspect.getmembers(cls, _is_method):
        if _has_caller(m):
            for ev_cls, _ in m.callers.items():
                service = getattr(sys.modules[ev_cls.__module__],
                                  '_SERVICE_NAME', None)
                if service:
                    # avoid cls that registers the own events (like
                    # ofp_handler)
                    if cls.__module__ != service:
                        services.append(service)

    m = sys.modules[cls.__module__]
    services.extend(getattr(m, '_REQUIRED_APP', []))
    services = list(set(services))
    return services


def register_service(service):
    frame = inspect.currentframe()
    m_name = frame.f_back.f_globals['__name__']
    m = sys.modules[m_name]
    m._SERVICE_NAME = service