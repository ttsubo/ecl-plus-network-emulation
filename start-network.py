import unittest
import nose
import os
import sys
import time
import logging
import json
from base.base import *

ESTABLISHED = 6

class Test(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        client1 = Container(name='frr-1', image='ttsubo/frrouting')
        client2 = FrroutingBGPContainer(name='inet-gw1', ctn_image_name='ttsubo/python_vrrp', asn=65003, router_id='10.10.10.3')
        client3 = FrroutingBGPContainer(name='inet-gw2', ctn_image_name='ttsubo/python_vrrp', asn=65003, router_id='10.10.10.4')
        client4 = FrroutingBGPContainer(name='frr-2', asn=65004, router_id='10.10.10.5')
        client5 = Container(name='ubuntu-1', image='ttsubo/frrouting')
        client6 = Container(name='ubuntu-2', image='ttsubo/frrouting')
        shared_network = [client1, client2, client3]
        hosts = [client1, client2, client3, client4, client5, client6]

        gobgp1 = GoPlaneContainer(name='gobgp1', asn=65000, router_id='10.0.0.1')
        gobgp2 = GoPlaneContainer(name='gobgp2', asn=65000, router_id='10.0.0.2')
        gobgp3 = GoPlaneContainer(name='gobgp3', asn=65000, router_id='10.0.0.3')
        gobgp4 = GoPlaneContainer(name='gobgp4', asn=65000, router_id='10.0.0.4')
        quagga1 = QuaggaBGPContainer(name='quagga1', asn=65001, router_id='10.10.10.1', zebra=True)
        quagga2 = QuaggaBGPContainer(name='quagga2', asn=65002, router_id='10.10.10.2', zebra=True)
        bgps = [gobgp1, gobgp2, gobgp3, gobgp4, quagga1, quagga2, client2, client3, client4]
        vtep10 = [gobgp1, gobgp3]
        vtep20 = [gobgp1, gobgp2]

        for idx, ctn in enumerate(vtep10):
            ctn.add_vn(10, 'vtep10', 10, ['eth3'])

        for idx, ctn in enumerate(vtep20):
            ctn.add_vn(20, 'vtep20', 20, ['eth4'])

        peer01 = [gobgp1, quagga1]
        peer02 = [gobgp2, quagga1]
        peer03 = [gobgp3, quagga1]
        peer04 = [gobgp4, quagga1]
        peer05 = [gobgp1, quagga2]
        peer06 = [gobgp2, quagga2]
        peer07 = [gobgp3, quagga2]
        peer08 = [gobgp4, quagga2]
        peer09 = [client2, client4]
        peer10 = [client3, client4]

        ctns = bgps + hosts
        [ctn.run() for ctn in ctns]

        br01 = Bridge(name='br-01', subnet='172.16.1.0/24')
        br01.addif(quagga1, 'eth1')
        br01.addif(gobgp1, 'eth1')

        br02 = Bridge(name='br-02', subnet='172.16.2.0/24')
        br02.addif(quagga1, 'eth2')
        br02.addif(gobgp2, 'eth1')

        br03 = Bridge(name='br-03', subnet='172.16.3.0/24')
        br03.addif(quagga1, 'eth3')
        br03.addif(gobgp3, 'eth1')

        br04 = Bridge(name='br-04', subnet='172.16.4.0/24')
        br04.addif(quagga1, 'eth4')
        br04.addif(gobgp4, 'eth1')

        br05 = Bridge(name='br-05', subnet='172.16.5.0/24')
        br05.addif(quagga2, 'eth1')
        br05.addif(gobgp1, 'eth2')

        br06 = Bridge(name='br-06', subnet='172.16.6.0/24')
        br06.addif(quagga2, 'eth2')
        br06.addif(gobgp2, 'eth2')

        br07 = Bridge(name='br-07', subnet='172.16.7.0/24')
        br07.addif(quagga2, 'eth3')
        br07.addif(gobgp3, 'eth2')

        br08 = Bridge(name='br-08', subnet='172.16.8.0/24')
        br08.addif(quagga2, 'eth4')
        br08.addif(gobgp4, 'eth2')

        for lfs, rfs in itertools.permutations(peer01, 2):
            lfs.add_peer(rfs, evpn=False)

        for lfs, rfs in itertools.permutations(peer02, 2):
            lfs.add_peer(rfs, evpn=False)

        for lfs, rfs in itertools.permutations(peer03, 2):
            lfs.add_peer(rfs, evpn=False)

        for lfs, rfs in itertools.permutations(peer04, 2):
            lfs.add_peer(rfs, evpn=False)

        for lfs, rfs in itertools.permutations(peer05, 2):
            lfs.add_peer(rfs, evpn=False)

        for lfs, rfs in itertools.permutations(peer06, 2):
            lfs.add_peer(rfs, evpn=False)

        for lfs, rfs in itertools.permutations(peer07, 2):
            lfs.add_peer(rfs, evpn=False)

        for lfs, rfs in itertools.permutations(peer08, 2):
            lfs.add_peer(rfs, evpn=False)

        gobgp1.add_peer_loopback(gobgp4, evpn=True)
        gobgp4.add_peer_loopback(gobgp1, evpn=True, RouteReflector=True)

        gobgp2.add_peer_loopback(gobgp4, evpn=True)
        gobgp4.add_peer_loopback(gobgp2, evpn=True, RouteReflector=True)

        gobgp3.add_peer_loopback(gobgp4, evpn=True)
        gobgp4.add_peer_loopback(gobgp3, evpn=True, RouteReflector=True)

        br09 = Bridge(name='br-09', with_ip=False)
        br09.addif(gobgp1, 'eth3')
        br09.addif(client1, 'eth0', 'aa:aa:aa:aa:aa:01')

        br10 = Bridge(name='br-10', with_ip=False)
        br10.addif(gobgp1, 'eth4')
        br10.addif(client1, 'eth4', 'aa:aa:aa:aa:aa:04')

        br11 = Bridge(name='br-11', with_ip=False)
        br11.addif(gobgp2, 'eth4')
        br11.addif(client6, 'eth0', 'aa:aa:aa:aa:aa:05')

        br12 = Bridge(name='br-12', with_ip=False)
        br12.addif(gobgp3, 'eth3')
        br12.addif(client2, 'eth0', 'aa:aa:aa:aa:aa:02')
        br12.addif(client3, 'eth0', 'aa:aa:aa:aa:aa:03')

        [ctn.local("ip a add 192.168.3.{0}/24 dev eth0".format(i+1)) for i, ctn in enumerate(shared_network)]
        client1.local("ip a add 192.168.4.1/24 dev eth4")
        client6.local("ip a add 192.168.4.2/24 dev eth0")

        br13 = Bridge(name='br-13', subnet='192.168.0.0/24')
        br13.addif(client2, 'eth1')
        br13.addif(client4, 'eth1')

        br14 = Bridge(name='br-14', subnet='192.168.1.0/24')
        br14.addif(client3, 'eth2')
        br14.addif(client4, 'eth2')

        br15 = Bridge(name='br-15', subnet='192.168.2.0/24')
        br15.addif(client4, 'eth0')
        br15.addif(client5, 'eth0')

        for lfs, rfs in itertools.permutations(peer09, 2):
            lfs.add_peer(rfs, evpn=False)

        for lfs, rfs in itertools.permutations(peer10, 2):
            lfs.add_peer(rfs, evpn=False)

        client2.add_peer_for_ibgp(client3, "192.168.3.3")
        client3.add_peer_for_ibgp(client2, "192.168.3.2")

        [ctn.start_goplane() for ctn in bgps]
        cls.ctns = {ctn.name: ctn for ctn in ctns}

        client1.add_static_route("0.0.0.0/0", "192.168.3.100")
        client2.add_static_route("192.168.4.0/24", "192.168.3.1")
        client3.add_static_route("192.168.4.0/24", "192.168.3.1")
        client5.add_static_route("0.0.0.0/0", "192.168.2.1")
        client6.add_static_route("0.0.0.0/0", "192.168.4.1")

    def test_01_neighbor_established(self):
        for i in range(300):
            if all(v['state']['session_state'] == ESTABLISHED for v in json.loads(self.ctns['gobgp4'].local('gobgp neighbor -j'))):
                    logging.debug('all peers got established')
                    return
            time.sleep(1)
        raise Exception('timeout')

if __name__ == '__main__':
    if os.geteuid() != 0:
        print("you are not root.")
        sys.exit(1)
    logging.basicConfig(stream=sys.stderr)
    nose.main(argv=sys.argv, defaultTest=sys.argv[0])
